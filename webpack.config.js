const HtmlWebpackPlugin = require('html-webpack-plugin');
const { resolve } = require('path');

const dev = process.env.NODE_ENV !== 'production';

/**
 * @type {import('webpack').Configuration}
 */
const config = {
  entry: './src/index.js',
  output: {
    path: resolve(__dirname, 'dist'),
    filename: '[name].js',
    clean: true,
  },
  mode: 'production',
  optimization: {
    minimize: !dev
  },
  performance: {
    assetFilter: () => false
  },
  resolve: {
    alias: {
      svelte: resolve('node_modules', 'svelte')
    },
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main']
  },
  module: {
    rules: [
      {
        test: /\.svelte$/,
        use: 'svelte-loader'
      },
      {
        test: /node_modules\/svelte\/.*\.mjs$/,
        resolve: {
          fullySpecified: false
        }
      }
    ]
  },
  devtool: dev ? 'source-map' : false,
  plugins: dev ? [
    new HtmlWebpackPlugin() 
  ] : [],
  devServer: {
    hot: true,
    port: 9000
  }
};

module.exports = config;

const rootVm = document.querySelector('#app').__vue__;
const http = rootVm.$http;
const log = rootVm.$message;

async function getBookList(hospitalId) {
  const user = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const res = await http.get(`nc-person/book-date/list/${user.id}/${hospitalId}`);
  return res.data;
}

async function submit({
  date,
  time,
  hospitalId
}) {
  const user = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const postData = {
    personId: user.id,
    bookingFlag: 2,
    bookingTime: `${date} ${time}`,
    hospitalId,
  };
  const res = await http.post('nc-person/add/booking', postData);
  return res;
}

export function action(hospitalId) {
  let count = 1;
  const timer = setInterval(async () => {
    const dateList = await getBookList(hospitalId);
    let isSelect = false;
    let date;
    let time;
    for (const resdate of dateList) {
      const { bookingDate, datePeriod } = resdate;
      for (const period of datePeriod) {
        const { bookingTime, surplusCounts, openFlag, timeFlag } = period;
        if (surplusCounts != 0 && openFlag != 0 && timeFlag != 0) {
          isSelect = true;
          date = bookingDate;
          time = bookingTime;
          break;
        }
      }
      if (isSelect) {
        break;
      }
    }
    if (isSelect && date && time) {
      const res = await submit({date, time, hospitalId});
      if (res.code == 200) {
        console.log('成功');
        log('成功');
        console.log(`已选${date} ${time}`);
        log(`已选${date} ${time}`);
        clearInterval(timer);
      }
    }
    console.log(`已选${count}次`);
    log(`已选${count}次`);
    count++;
  }, 500);
  return timer;
}
